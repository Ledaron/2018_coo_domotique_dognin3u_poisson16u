
public class Lampe extends Appareil{
	private String nom;
	private Boolean allume;
	
	public Lampe(String n){
		this.nom=n;
		this.allume=false;
	}
	
	public void allumer(){
		this.allume=true;
	}
	
	public void eteindre(){
		this.allume=false;
	}
	
	public boolean etat(){
		return this.allume;
	}
	
	public String toString(){
		String s= "je suis la lampe :"+this.nom+" et je suis "+this.allume;
		return s;
	}
}
