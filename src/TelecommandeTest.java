import static org.junit.Assert.*;
import org.junit.Test;

public class TelecommandeTest {
	
	@Test
	/**
	 * Test du constructeur telecommande vide
	 */
	public void testTelecommande(){
		// methode testee
				Lampe l= new Lampe("l1");
				Telecommande t = new Telecommande();
				
				t.ajouter(l);
				// verification
				String r =""+t;
				assertEquals("une nouvelle lampe ", "telecommande controle [je suis la lampe :l1 et je suis false]", r);
			}

			@Test
			/**
			 * test ajout lampe a telecommande avec 1 element
			 */
			public void testAjoutLampe_telecommandeAvec1Element() {
				// preparation des donnees
				Lampe l = new Lampe("l1");
				Lampe l2 = new Lampe("l2");
				Telecommande t = new Telecommande();
				
				t.ajouter(l);
				t.ajouter(l2);
				
				//methode testee
				String r =""+t;
				
				// verification
				
				assertEquals("une nouvelle lampe ", "telecommande controle [je suis la lampe :l1 et je suis false, je suis la lampe :l2 et je suis false]", r);
			}

			@Test
			/**
			 * test eteindre une lampe allumee
			 */
			public void testeteindre_allumee() {
				// preparation des donnees
				Lampe l1 = new Lampe("lampe1");
				Lampe l2 = new Lampe("lampe2");
				Telecommande t = new Telecommande();
				t.ajouter(l1);
				t.ajouter(l2);
				//methode testee
				t.activer(0);
				String r=""+l1;
				//verification
				assertEquals("affichage devrait etre true","je suis la lampe :lampe1 et je suis true", r);
			}
			
			@Test
			/**
			 * test to String lampe eteinte
			 */
			public void testactiverpos1() {
				// preparation des donnees
				Lampe l1 = new Lampe("lampe1");
				Lampe l2 = new Lampe("lampe2");
				Telecommande t = new Telecommande();
				t.ajouter(l1);
				t.ajouter(l2);
				//methode testee
				t.activer(1);
				String r=""+l2;
				//verification
				assertEquals("affichage devrait etre true","je suis la lampe :lampe2 et je suis true", r);
			}
			
			@Test
			/**
			 * test to String lampe allumee
			 */
			public void testlampe_inexistante() {
				Telecommande t = new Telecommande();
				//methode testee
				t.activer(1);
				String r= ""+t;
				//verification
				assertEquals("une erreur devrait etre levee ","lampe non connue",r);

			}

	
}