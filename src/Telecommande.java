import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;

public class Telecommande {
	private List<Appareil> appareils;
	
	
	public Telecommande(){
		this.appareils = new ArrayList<Appareil>();
	}
	
	public void ajouter(Lampe l){
		int lg= this.appareils.size();
		this.appareils.add(lg,l) ;
	}
	
	public void activer(int indiceLampe){
	
		appareils.get(indiceLampe).allumer();
	}
	
	public void desactiver(int indiceLampe){
		appareils.get(indiceLampe).eteindre();
	}
	
	public void activerTout(){
		for (int i=0;i<this.appareils.size();i++)
			this.activer(i);	
	}

	public String toString(){
		String s= "telecommande controle " + this.appareils;
		return s;
	}
}
